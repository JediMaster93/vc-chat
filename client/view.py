"""
gui stuff
"""

import Tkinter
from Tkinter import *
class ChatWindow(Frame):
  
    def __init__(self, parent):
        Frame.__init__(self, parent, background="white")   
         
        self.parent = parent
        
        self.initUI()
    
    def initUI(self):
      
        self.parent.title("Simple")
        self.pack(fill=BOTH, expand=1)
        
        mainPanel = Frame(self)
        bottomPanel = Frame(self)

        
        self.chatBox = Tkinter.Text(mainPanel)
        self.chatBox.config(highlightthickness = 1)
        scrollbar = Tkinter.Scrollbar(mainPanel)
        scrollbar.config(command = self.chatBox.yview)
        self.chatBox.config(yscrollcommand = scrollbar.set)
        scrollbar.pack(side = RIGHT, fill = Y)
        
        self.chatBox.pack(side = LEFT, fill = BOTH,expand = 1)
        mainPanel.config(highlightthickness = 1)
        bottomPanel.config(highlightthickness = 1)
        
        
        
        
        inputTextField = Entry(bottomPanel)
        inputTextField.insert(END, "inser text here")
        sendButton = Button(bottomPanel,text="send")
        sendButton.pack(side = RIGHT)
        inputTextField.pack(fill = X)
        
        bottomPanel.pack(side = BOTTOM, fill = X)
        mainPanel.pack(fill = BOTH,expand = 1)
        

    def insertText(self, text):
        self.chatBox.insert(Tkinter.END, text + "\n")   

def main():
  
    root = Tk()
    root.geometry("800x600+300+300")
    app = ChatWindow(root)
    root.mainloop()  


if __name__ == '__main__':
    main()  