"""
Server recieves requests and proxies them to all other clients.
"""
#figure out how to make a server lol
import socket
import sys
import threading

class Server(object):

    def __init__(self):
        self.connectionList =[]
        pass

    def startServer(self,ip,port):
        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serverAdress = (ip,port)

        self.serverSocket.bind(self.serverAdress)


    def waitForConnection(self):
        self.serverSocket.listen(1)
        while True:
            #spawn thread for handling clients
            connection, clientAdress = self.serverSocket.accept()
            self.connectionList.append(connection)

            t = threading.Thread(target = self.handleClient, args = (connection,))
            self.threads = []
            self.threads.append(t)
            t.start()

    def handleClient(self, connection):
        while True:
            try:
                data = connection.recv(16)
                for connection in self.connectionList:
                    print "server sending to conn", connection.getsockname(), ">>>", data
                    connection.sendall("server multicast " + data)
            except:
                print "closing connection  because of exception"
                connection.close()
                break

if __name__ == "__main__":
    s = Server()
    s.startServer("0.0.0.0", 8888)
    s.waitForConnection()
